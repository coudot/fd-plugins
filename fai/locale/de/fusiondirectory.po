# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:53+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: German (https://www.transifex.com/fusiondirectory/teams/12202/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/fai/class_faiVariable.inc:26
msgid "Variable"
msgstr "Variable"

#: admin/fai/class_faiVariable.inc:27 admin/fai/class_faiVariable.inc:32
msgid "FAI variable"
msgstr "FAI-Variable"

#: admin/fai/class_faiVariable.inc:47 admin/fai/class_faiScript.inc:47
#: admin/fai/class_faiProfile.inc:46 admin/fai/class_faiPackage.inc:99
#: admin/fai/class_faiHook.inc:47 admin/fai/class_faiTemplate.inc:165
#: admin/fai/class_faiTemplateEntry.inc:139
#: admin/fai/class_faiPartitionTable.inc:337
msgid "Properties"
msgstr "Eigenschaften"

#: admin/fai/class_faiVariable.inc:50 admin/fai/class_faiScript.inc:50
#: admin/fai/class_faiProfile.inc:49 admin/fai/class_faiPackage.inc:102
#: admin/fai/class_faiHook.inc:50 admin/fai/class_faiTemplate.inc:168
#: admin/fai/class_faiPartitionTable.inc:340
msgid "Class name"
msgstr "Klassen-Name"

#: admin/fai/class_faiVariable.inc:50 admin/fai/class_faiScript.inc:50
#: admin/fai/class_faiPackage.inc:102 admin/fai/class_faiHook.inc:50
msgid "Variables class name"
msgstr "Variablenklassenname"

#: admin/fai/class_faiVariable.inc:54 admin/fai/class_faiVariable.inc:72
#: admin/fai/class_faiDiskEntry.inc:246 admin/fai/class_faiPartition.inc:246
#: admin/fai/class_faiScript.inc:54 admin/fai/class_faiScript.inc:72
#: admin/fai/class_faiProfile.inc:53 admin/fai/class_faiPackage.inc:106
#: admin/fai/class_faiHook.inc:54 admin/fai/class_faiHook.inc:72
#: admin/fai/class_faiTemplate.inc:172
#: admin/fai/class_faiTemplateEntry.inc:146
#: admin/fai/class_faiPartitionTable.inc:344
msgid "Description"
msgstr "Beschreibung"

#: admin/fai/class_faiVariable.inc:54 admin/fai/class_faiScript.inc:54
#: admin/fai/class_faiProfile.inc:53 admin/fai/class_faiPackage.inc:106
#: admin/fai/class_faiHook.inc:54 admin/fai/class_faiTemplate.inc:172
#: admin/fai/class_faiPartitionTable.inc:344
msgid "Short description of the class"
msgstr "Kurze Beschreibung von der Klasse"

#: admin/fai/class_faiVariable.inc:60
msgid "Variables"
msgstr "Variablen"

#: admin/fai/class_faiVariable.inc:64 admin/fai/class_faiScript.inc:64
#: admin/fai/class_faiHook.inc:64
msgid "Variables in this class"
msgstr "Variablen in dieser Klasse"

#: admin/fai/class_faiVariable.inc:68
#: admin/fai/class_faiSimplePluginClass.inc:51
#: admin/fai/class_faiDiskEntry.inc:240 admin/fai/class_faiPartition.inc:240
#: admin/fai/class_faiScript.inc:68 admin/fai/class_faiHook.inc:68
msgid "Name"
msgstr "Name"

#: admin/fai/class_faiVariable.inc:68 admin/fai/class_faiScript.inc:68
#: admin/fai/class_faiHook.inc:68
msgid "The name of the variable"
msgstr "Der Name der Variable"

#: admin/fai/class_faiVariable.inc:72 admin/fai/class_faiScript.inc:72
#: admin/fai/class_faiHook.inc:72
msgid "A short description of the variable"
msgstr "Eine kurze Beschreibung von der Variable"

#: admin/fai/class_faiVariable.inc:76
msgid "Content"
msgstr "Inhalt"

#: admin/fai/class_faiVariable.inc:76
msgid "The content of the variable"
msgstr "Der Inhalt der Variable"

#: admin/fai/class_faiDiskEntry.inc:101
msgid "bootable"
msgstr "bootbar"

#: admin/fai/class_faiDiskEntry.inc:104
msgid "preserve"
msgstr "bewahren"

#: admin/fai/class_faiDiskEntry.inc:146 admin/systems/class_faiLogView.inc:200
#: admin/systems/class_faiLogView.inc:242
msgid "Error"
msgstr "Fehler"

#: admin/fai/class_faiDiskEntry.inc:147
#, php-format
msgid ""
"The partition cannot be deleted while it is used in the \"%s\" disk "
"definition!"
msgstr ""

#: admin/fai/class_faiDiskEntry.inc:225
msgid "Partition table entry"
msgstr "Partitionstabellen-Eintrag"

#: admin/fai/class_faiDiskEntry.inc:226
msgid "FAI partition table entry"
msgstr "FAI-Partitionstabellen-Eintrag"

#: admin/fai/class_faiDiskEntry.inc:237 admin/fai/class_faiDiskEntry.inc:253
msgid "Device"
msgstr "Gerät"

#: admin/fai/class_faiDiskEntry.inc:240
msgid "Disk name"
msgstr "Festplattenname"

#: admin/fai/class_faiDiskEntry.inc:246 admin/fai/class_faiPartition.inc:246
msgid "Short description"
msgstr "Kurze Beschreibung"

#: admin/fai/class_faiDiskEntry.inc:250
msgid "fstab key"
msgstr "fstab-Schlüssel"

#: admin/fai/class_faiDiskEntry.inc:250
msgid "Key to use in fstab file"
msgstr ""

#: admin/fai/class_faiDiskEntry.inc:253
msgid "Label"
msgstr "Label"

#: admin/fai/class_faiDiskEntry.inc:253
msgid "UUID"
msgstr "UUID"

#: admin/fai/class_faiDiskEntry.inc:256
msgid "Disk label"
msgstr "Disk-Label"

#: admin/fai/class_faiDiskEntry.inc:262
msgid "Random init"
msgstr ""

#: admin/fai/class_faiDiskEntry.inc:262
msgid "Initialise all encrypted partitions with random data"
msgstr ""

#: admin/fai/class_faiDiskEntry.inc:269
msgid "Partition entries"
msgstr "Partitions-Einträge"

#: admin/fai/class_faiDiskEntry.inc:274 admin/fai/class_faiPartition.inc:452
msgid "Combined physical partitions"
msgstr "Kombinierte physikalische Partitionen"

#: admin/fai/class_faiDiskEntry.inc:274
msgid "Physical partitions combined in this LVM volume"
msgstr ""

#: admin/fai/class_faiDiskEntry.inc:280
#: admin/fai/class_faiPartitionTable.inc:357
msgid "Partitions in this class"
msgstr "Partitionen in dieser Klasse"

#: admin/fai/class_faiDiskEntry.inc:578
msgid ""
"You have more than four primary partition table entries in your "
"configuration"
msgstr ""

#: admin/fai/class_faiDiskEntry.inc:582
msgid ""
"You cannot have more than three primary partition while using logical "
"partitions"
msgstr ""

#: admin/fai/packageSelect/class_packageSelectManagement.inc:31
#: admin/fai/class_faiPackage.inc:78
msgid "Package"
msgstr "Paket"

#: admin/fai/packageSelect/class_packageSelectManagementFilter.inc:50
#: admin/fai/packageSelect/class_packageSelectManagementFilter.inc:55
#: admin/fai/packageSelect/class_PackagesAttribute.inc:245
#: admin/fai/class_faiPackageConfiguration.inc:59
msgid "Infrastructure service"
msgstr "Infrastrukturdienst"

#: admin/fai/packageSelect/class_PackagesAttribute.inc:121
msgid "Mark for installation"
msgstr "Zur Installation markieren"

#: admin/fai/packageSelect/class_PackagesAttribute.inc:125
msgid "Mark for removal"
msgstr "Zum entfernen vormerken"

#: admin/fai/packageSelect/class_PackagesAttribute.inc:130
msgid "Edit configuration"
msgstr "Konfiguration bearbeiten"

#: admin/fai/packageSelect/class_PackagesAttribute.inc:209
#: admin/fai/class_faiTemplate.inc:109
#: admin/fai/class_faiPartitionTable.inc:246
#: admin/fai/class_faiPartitionTable.inc:287
#: admin/systems/services/repository/class_serviceRepository.inc:240
#: admin/systems/services/repository/class_serviceRepository.inc:257
#: admin/systems/services/repository/class_serviceRepository.inc:261
msgid "LDAP error"
msgstr "LDAP-Fehler"

#: admin/fai/packageSelect/class_PackagesAttribute.inc:264
msgid "Unkown packages"
msgstr "Unbekannte Pakete"

#: admin/fai/packageSelect/class_PackageSelectCustomFilterElement.inc:57
#: admin/fai/packageSelect/class_PackageSelectCustomFilterElement.inc:58
#, php-format
msgid "Custom %s"
msgstr ""

#: admin/fai/packageSelect/class_PackageSelectCustomFilterElement.inc:64
msgid "Customs"
msgstr ""

#: admin/fai/class_faiPartition.inc:30
msgid "Type of sizing - fixed, dynamic or all remaining space"
msgstr ""

#: admin/fai/class_faiPartition.inc:33
msgid "Fixed"
msgstr "Fest"

#: admin/fai/class_faiPartition.inc:33
msgid "Dynamic"
msgstr "Dynamisch"

#: admin/fai/class_faiPartition.inc:33
msgid "Remaining space"
msgstr "Verbleibender Platz"

#: admin/fai/class_faiPartition.inc:44 admin/fai/class_faiPartition.inc:55
msgid "KB"
msgstr "KB"

#: admin/fai/class_faiPartition.inc:44 admin/fai/class_faiPartition.inc:55
msgid "MB"
msgstr "MB"

#: admin/fai/class_faiPartition.inc:44 admin/fai/class_faiPartition.inc:55
msgid "GB"
msgstr "GB"

#: admin/fai/class_faiPartition.inc:44 admin/fai/class_faiPartition.inc:55
msgid "TB"
msgstr "TB"

#: admin/fai/class_faiPartition.inc:44 admin/fai/class_faiPartition.inc:55
msgid "PB"
msgstr "PB"

#: admin/fai/class_faiPartition.inc:144
msgid "Minimum partition size"
msgstr "Partitionsgröße"

#: admin/fai/class_faiPartition.inc:144
msgid "Maximum partition size"
msgstr "Partitionsgröße"

#: admin/fai/class_faiPartition.inc:217
msgid "Partition entry"
msgstr "Partitionseintrag"

#: admin/fai/class_faiPartition.inc:218
msgid "FAI partition entry"
msgstr "FAI-Partitionseintrag"

#: admin/fai/class_faiPartition.inc:229 admin/fai/class_faiPartition.inc:292
msgid "Partition"
msgstr "Partition"

#: admin/fai/class_faiPartition.inc:234
msgid "Type"
msgstr "Typ"

#: admin/fai/class_faiPartition.inc:234
msgid "Partition type"
msgstr "Partitionstyp"

#: admin/fai/class_faiPartition.inc:237 admin/fai/class_faiPartition.inc:379
msgid "Primary"
msgstr "primär"

#: admin/fai/class_faiPartition.inc:237 admin/fai/class_faiPartition.inc:376
#: admin/fai/class_faiPartition.inc:380
msgid "Logical"
msgstr "logisch"

#: admin/fai/class_faiPartition.inc:240
msgid "Partition name"
msgstr "Partitionsname"

#: admin/fai/class_faiPartition.inc:250
msgid "Size"
msgstr "Größe"

#: admin/fai/class_faiPartition.inc:250
msgid "Size of this partition"
msgstr "Größe dieser Partition"

#: admin/fai/class_faiPartition.inc:256
msgid "Flags"
msgstr "Flags"

#: admin/fai/class_faiPartition.inc:265
msgid "Combined physical partitions in this RAID"
msgstr ""

#: admin/fai/class_faiPartition.inc:269
msgid "Spare"
msgstr ""

#: admin/fai/class_faiPartition.inc:269
msgid "Spare Raid device"
msgstr ""

#: admin/fai/class_faiPartition.inc:275
msgid "Missing"
msgstr "Fehlt"

#: admin/fai/class_faiPartition.inc:275
msgid "Missing Raid device"
msgstr ""

#: admin/fai/class_faiPartition.inc:292
msgid "Partition to encrypt"
msgstr ""

#: admin/fai/class_faiPartition.inc:296
msgid "Password"
msgstr "Passwort"

#: admin/fai/class_faiPartition.inc:296
msgid ""
"Password to use for encryption. Leave empty to use a encryption key file "
"instead."
msgstr ""

#: admin/fai/class_faiPartition.inc:300
msgid "Resize"
msgstr "Größe ändern"

#: admin/fai/class_faiPartition.inc:300
msgid "Resize existing partition"
msgstr "Existierende Partition in Größe anpassen"

#: admin/fai/class_faiPartition.inc:304
msgid "Bootable"
msgstr "Bootbar"

#: admin/fai/class_faiPartition.inc:304
msgid "Wether or not this partition can be booted"
msgstr ""

#: admin/fai/class_faiPartition.inc:308
msgid "Preserve"
msgstr "Bewahren"

#: admin/fai/class_faiPartition.inc:308
msgid "Does the partition need to be preserved"
msgstr ""

#: admin/fai/class_faiPartition.inc:311
msgid "Never"
msgstr "Nie"

#: admin/fai/class_faiPartition.inc:311
msgid "Always"
msgstr "Immer"

#: admin/fai/class_faiPartition.inc:311
msgid "Reinstall"
msgstr "Erneut installieren"

#: admin/fai/class_faiPartition.inc:316 admin/fai/class_faiPartition.inc:319
msgid "Filesystem"
msgstr "Dateisystem"

#: admin/fai/class_faiPartition.inc:319
msgid "The filesystem this partition should be formatted with"
msgstr "Das Dateisystem dieser Partition sollte formatiert werden mit"

#: admin/fai/class_faiPartition.inc:322
msgid "swap"
msgstr ""

#: admin/fai/class_faiPartition.inc:322
msgid "fat32"
msgstr ""

#: admin/fai/class_faiPartition.inc:322
msgid "ext2"
msgstr "ext2"

#: admin/fai/class_faiPartition.inc:322
msgid "ext3"
msgstr "ext3"

#: admin/fai/class_faiPartition.inc:322
msgid "ext4"
msgstr "ext4"

#: admin/fai/class_faiPartition.inc:322
msgid "reiser fs"
msgstr "reiser fs"

#: admin/fai/class_faiPartition.inc:322
msgid "xfs"
msgstr "xfs"

#: admin/fai/class_faiPartition.inc:322
msgid "btrfs"
msgstr "btrfs"

#: admin/fai/class_faiPartition.inc:322
msgid "LVM/RAID"
msgstr "LVM/RAID"

#: admin/fai/class_faiPartition.inc:325
msgid "Mount point"
msgstr "Einhänge-Pfad"

#: admin/fai/class_faiPartition.inc:325
msgid "Mount point for this partition"
msgstr "Einhängepunkt für diese Partition"

#: admin/fai/class_faiPartition.inc:331
msgid "Options"
msgstr "Optionen"

#: admin/fai/class_faiPartition.inc:334
msgid "Mount options"
msgstr "Mount-Optionen"

#: admin/fai/class_faiPartition.inc:334
msgid "Filesystem mount options"
msgstr ""

#: admin/fai/class_faiPartition.inc:339
msgid "Create options"
msgstr "Optionen aktivieren"

#: admin/fai/class_faiPartition.inc:339
msgid "Filesystem create options"
msgstr "Dateisystem Erstell-Optionen"

#: admin/fai/class_faiPartition.inc:343
msgid "Tune options"
msgstr "Optimierungs-Optionen"

#: admin/fai/class_faiPartition.inc:343
msgid "Filesystem tune options"
msgstr ""

#: admin/fai/class_faiPartition.inc:385
msgid "RAID 0"
msgstr "RAID 0"

#: admin/fai/class_faiPartition.inc:386
msgid "RAID 1"
msgstr "RAID 1"

#: admin/fai/class_faiPartition.inc:387
msgid "RAID 5"
msgstr "RAID 5"

#: admin/fai/class_faiPartition.inc:388
msgid "RAID 6"
msgstr "RAID 6"

#: admin/fai/class_faiPartition.inc:392
msgid "LUKS"
msgstr ""

#: admin/fai/class_faiPartition.inc:393
msgid "Swap"
msgstr ""

#: admin/fai/class_faiPartition.inc:394
msgid "tmp"
msgstr ""

#: admin/fai/class_faiPartition.inc:434
msgid "Encryption settings"
msgstr ""

#: admin/fai/class_faiPartition.inc:507
msgid "Raid arrays must contain at least two partitions!"
msgstr "RAID-Arrays müssen mindestens zwei Partitionen enthalten!"

#: admin/fai/class_faiPartition.inc:509
msgid ""
"Raid 0 arrays can only be realized with a combination of two partitions!"
msgstr ""
"RAID 0 Arrays können nur in Kombination von zwei Partitionen realisiert "
"werden!"

#: admin/fai/class_faiScript.inc:26 admin/fai/class_faiScript.inc:81
#: admin/fai/class_faiHook.inc:84
msgid "Script"
msgstr "Skript"

#: admin/fai/class_faiScript.inc:27 admin/fai/class_faiScript.inc:32
msgid "FAI script"
msgstr "FAI-Skript"

#: admin/fai/class_faiScript.inc:60
msgid "Scripts"
msgstr "Skripte"

#: admin/fai/class_faiScript.inc:76
msgid "Priority"
msgstr "Priorität"

#: admin/fai/class_faiScript.inc:76
msgid "Priority of this script (smaller is done first)"
msgstr ""

#: admin/fai/class_faiScript.inc:81
msgid "The script itself"
msgstr "Das Skript selbst"

#: admin/fai/class_faiProfile.inc:26
msgid "Profile"
msgstr "Profil"

#: admin/fai/class_faiProfile.inc:27
msgid "FAI profile"
msgstr "FAI-Profil"

#: admin/fai/class_faiProfile.inc:32
msgid "FAI Profile"
msgstr "FAI Profil"

#: admin/fai/class_faiProfile.inc:49 admin/fai/class_faiPartitionTable.inc:340
msgid "Partition table class name"
msgstr "Partitionstabellen-Klassenname"

#: admin/fai/class_faiProfile.inc:60
msgid "FAI classes"
msgstr "FAI-Klassen"

#: admin/fai/class_faiProfile.inc:60
msgid "FAI classes which are part of this profile"
msgstr "FAI-Klassen, die Teil dieses Profils sind"

#: admin/fai/class_faiPackageConfiguration.inc:33
#: admin/fai/class_faiPackageConfiguration.inc:36
msgid "FAI package configuration"
msgstr ""

#: admin/fai/class_faiPackageConfiguration.inc:34
msgid "Configure debconf options of a package"
msgstr ""

#: admin/fai/class_faiPackageConfiguration.inc:80
#, php-format
msgid "Debconf information for package \"%s\""
msgstr ""

#: admin/fai/class_faiPackageConfiguration.inc:172
msgid "Warning"
msgstr "Warnung"

#: admin/fai/class_faiPackageConfiguration.inc:173
#, php-format
msgid ""
"The debconf questions \"%s\" are dynamically generated during package "
"installation and requires choosing between specific options which cannot be "
"presented here. The entered text needs to be one of the valid choices in "
"order to take effect."
msgstr ""

#: admin/fai/class_faiPackage.inc:79
msgid "FAI Package list"
msgstr "FAI-Paketliste"

#: admin/fai/class_faiPackage.inc:84
msgid "FAI Package"
msgstr "FAI Paket"

#: admin/fai/class_faiPackage.inc:112 admin/systems/class_faiStartup.inc:101
msgid "Repository"
msgstr "Repository"

#: admin/fai/class_faiPackage.inc:115 admin/systems/class_faiStartup.inc:89
#: admin/systems/services/repository/class_serviceRepository.inc:40
msgid "Release"
msgstr "Release"

#: admin/fai/class_faiPackage.inc:115
msgid "Debian release concerned"
msgstr "Debian-Version betreffend"

#: admin/fai/class_faiPackage.inc:119
#: admin/systems/services/repository/class_serviceRepository.inc:47
msgid "Sections"
msgstr "Sektionen"

#: admin/fai/class_faiPackage.inc:119
msgid "Sections concerned"
msgstr ""

#: admin/fai/class_faiPackage.inc:123
msgid "Install method"
msgstr "Installations-Methode"

#: admin/fai/class_faiPackage.inc:123
msgid "Install method to use for this package list"
msgstr "Für diese Paketliste zu verwendende Installationsmethode"

#: admin/fai/class_faiPackage.inc:134
msgid "Packages"
msgstr "Pakete"

#: admin/fai/class_faiPackage.inc:138
msgid "Packages in this class"
msgstr "Pakete in dieser Klasse"

#: admin/fai/class_faiHook.inc:26
msgid "Hook"
msgstr "Hook"

#: admin/fai/class_faiHook.inc:27 admin/fai/class_faiHook.inc:32
msgid "FAI hook"
msgstr "FAI-Hook"

#: admin/fai/class_faiHook.inc:60
msgid "Hooks"
msgstr "Hooks"

#: admin/fai/class_faiHook.inc:76
msgid "Task"
msgstr "Vorgang"

#: admin/fai/class_faiHook.inc:76
msgid "Task to run"
msgstr ""

#: admin/fai/class_faiHook.inc:84
msgid "The script of this hook"
msgstr ""

#: admin/fai/class_faiTemplate.inc:131
#, php-format
msgid "There are several files with the same path: %s"
msgstr "Es gibt mehrere Dateien mit dem gleichen Pfad: %s"

#: admin/fai/class_faiTemplate.inc:144
msgid "Template"
msgstr "Vorlage"

#: admin/fai/class_faiTemplate.inc:145 admin/fai/class_faiTemplate.inc:150
msgid "FAI template"
msgstr "FAI-Vorlage"

#: admin/fai/class_faiTemplate.inc:168
msgid "Template class name"
msgstr "Vorlagenklassenname"

#: admin/fai/class_faiTemplate.inc:178
msgid "Template files"
msgstr "Vorlagendateien"

#: admin/fai/class_faiTemplate.inc:182
msgid "Template files in this class"
msgstr "Vorlagendateien in dieser Klasse"

#: admin/fai/class_faiManagement.inc:27 admin/fai/class_faiManagement.inc:34
#: admin/systems/class_faiStartup.inc:61 config/fai/class_faiConfig.inc:27
#: config/fai/class_faiConfig.inc:42
msgid "FAI"
msgstr "FAI"

#: admin/fai/class_faiManagement.inc:28
msgid "Manage FAI software packages and deployment recipes"
msgstr "Verwaltung von FAI Softwarepaketen und Deployment-Recipes"

#: admin/fai/class_faiManagement.inc:62
msgid "There are no FAI branches"
msgstr "Es gibt keine FAI-Zweige"

#: admin/fai/class_faiManagement.inc:65
msgid " Please add at least one repository service to create those."
msgstr ""

#: admin/fai/class_faiTemplateEntry.inc:142
msgid "File path"
msgstr "Dateipfad"

#: admin/fai/class_faiTemplateEntry.inc:142
msgid "Complete absolute template file path and name"
msgstr "Vollständiger absoluter Vorlagendateipfad und -name"

#: admin/fai/class_faiTemplateEntry.inc:146
msgid "Short description of the file"
msgstr "Kurze Beschreibung von der Datei"

#: admin/fai/class_faiTemplateEntry.inc:155
msgid "Template attributes"
msgstr "Vorlagen-Attribute"

#: admin/fai/class_faiTemplateEntry.inc:159
msgid "Template file content"
msgstr "Vorlagendateiinhalt"

#: admin/fai/class_faiTemplateEntry.inc:159
msgid "Content of the template file"
msgstr "Inhalt der Vorlagendatei"

#: admin/fai/class_faiTemplateEntry.inc:163
msgid "Owner and group of the file"
msgstr "Eigentümer und Gruppe von der Datei"

#: admin/fai/class_faiTemplateEntry.inc:166
msgid "Owner"
msgstr "Besitzer"

#: admin/fai/class_faiTemplateEntry.inc:166
msgid "File owner"
msgstr "Datei-Besitzer"

#: admin/fai/class_faiTemplateEntry.inc:171
msgid "Group"
msgstr "Gruppe"

#: admin/fai/class_faiTemplateEntry.inc:171
msgid "File group"
msgstr "Dateigruppe"

#: admin/fai/class_faiTemplateEntry.inc:180
msgid "Access"
msgstr "Zugriff"

#: admin/fai/class_faiTemplateEntry.inc:180
msgid "Access rights"
msgstr "Zugriffsrechte"

#: admin/fai/class_faiPartitionTable.inc:105
msgid "Add disk"
msgstr ""

#: admin/fai/class_faiPartitionTable.inc:115
msgid "Add RAID"
msgstr "RAID hinzufügen"

#: admin/fai/class_faiPartitionTable.inc:126
msgid "Add LVM"
msgstr "LVM hinzufügen"

#: admin/fai/class_faiPartitionTable.inc:137
msgid "Add cryptsetup"
msgstr ""

#: admin/fai/class_faiPartitionTable.inc:316
msgid "Partition table"
msgstr "Partitionstabelle"

#: admin/fai/class_faiPartitionTable.inc:317
#: admin/fai/class_faiPartitionTable.inc:322
msgid "FAI partition table"
msgstr "FAI-Partitionstabelle"

#: admin/fai/class_faiPartitionTable.inc:350
msgid "Discs"
msgstr "Festplatten"

#: admin/systems/class_faiLogView.inc:57
msgid "Sort down"
msgstr "Absteigend sortieren"

#: admin/systems/class_faiLogView.inc:59
msgid "Sort up"
msgstr "Aufsteigend sortieren"

#: admin/systems/class_faiLogView.inc:71
msgid "File"
msgstr "Datei"

#: admin/systems/class_faiLogView.inc:72
msgid "Date"
msgstr "Datum"

#: admin/systems/class_faiLogView.inc:148
msgid "FAI Logs"
msgstr "FAI-Protokolle"

#: admin/systems/class_faiLogView.inc:149
msgid "FAI Logs Viewer"
msgstr "FAI-Protokollbetrachter"

#: admin/systems/class_faiLogView.inc:161
#: admin/systems/class_faiLogView.inc:164
msgid "Available logs"
msgstr "Verfügbare Protokolle"

#: admin/systems/class_faiLogView.inc:169
msgid "Selected log"
msgstr "Ausgewähltes Protokoll"

#: admin/systems/class_faiLogView.inc:173
msgid "Content of the selected log"
msgstr "Inhalt des ausgewählten Protokolls"

#: admin/systems/class_faiLogView.inc:222
msgid "Selected file"
msgstr "Ausgewählte Datei"

#: admin/systems/class_faiLogView.inc:230
#, php-format
msgid "Selected file: %s"
msgstr "Ausgewählte Datei: %s"

#: admin/systems/class_faiStartup.inc:62
msgid "Full automated installation"
msgstr "Full Automated Installation"

#: admin/systems/class_faiStartup.inc:81
msgid "FAI settings"
msgstr "FAI-Einstellungen"

#: admin/systems/class_faiStartup.inc:85
msgid "FAI profil and release to be applied to this computer"
msgstr ""
"FAI Profil und Veröffentlichung, die auf diesen Rechner angewendet werden "
"soll"

#: admin/systems/class_faiStartup.inc:89
msgid "FAI debian release to be installed on this computer"
msgstr ""
"FAI Debian-Veröffentlichung, die auf diesen Rechner installiert werden soll"

#: admin/systems/class_faiStartup.inc:93
msgid "Profil"
msgstr "Profil"

#: admin/systems/class_faiStartup.inc:93
msgid "FAI profil to be applied to this computer"
msgstr "FAI-Profil welches auf diesem Rechner angewendet werden soll"

#: admin/systems/class_faiStartup.inc:101
msgid "FAI Debian repository to be used for installation"
msgstr ""
"FAI Debian-Repository, welches für die Installation verwendet werden soll"

#: admin/systems/services/repository/class_serviceRepository.inc:31
msgid "URL"
msgstr "URL"

#: admin/systems/services/repository/class_serviceRepository.inc:31
msgid "Repository url"
msgstr "Repository URL"

#: admin/systems/services/repository/class_serviceRepository.inc:35
msgid "Parent server"
msgstr "Quell-Server"

#: admin/systems/services/repository/class_serviceRepository.inc:35
msgid "Parent repository server"
msgstr "Parent Repository Server"

#: admin/systems/services/repository/class_serviceRepository.inc:40
msgid "Release used on this repository"
msgstr ""

#: admin/systems/services/repository/class_serviceRepository.inc:47
msgid "Sections available on this repository"
msgstr ""

#: admin/systems/services/repository/class_serviceRepository.inc:52
msgid "Mirror mode"
msgstr "Mirror-Modus"

#: admin/systems/services/repository/class_serviceRepository.inc:52
msgid ""
"Is this mirror an installation repository? Is its release a custom one?"
msgstr ""
"Ist dieser Mirror ein Installationsrepository? Ist sein Release ein eigenes "
"erstelltes?"

#: admin/systems/services/repository/class_serviceRepository.inc:57
msgid "Local mirror"
msgstr "Lokaler Mirror"

#: admin/systems/services/repository/class_serviceRepository.inc:57
msgid "Is this mirror a local or a network mirror?"
msgstr "Ist dieser Mirror ein lokaler oder ein Netzwerk-Mirror?"

#: admin/systems/services/repository/class_serviceRepository.inc:66
msgid "Architectures"
msgstr "Architekturen"

#: admin/systems/services/repository/class_serviceRepository.inc:66
msgid "Processor architectures available on this repository"
msgstr "Verfügbare Prozessorarchitekturen in diesem Repository"

#: admin/systems/services/repository/class_serviceRepository.inc:72
msgid "Distribution"
msgstr "Verteilung"

#: admin/systems/services/repository/class_serviceRepository.inc:72
msgid "Which distribution is this repository for"
msgstr ""

#: admin/systems/services/repository/class_serviceRepository.inc:77
msgid "Path pattern"
msgstr "Pfadmuster"

#: admin/systems/services/repository/class_serviceRepository.inc:77
msgid "How this repository is organized (only for RPM repositories)"
msgstr ""

#: admin/systems/services/repository/class_serviceRepository.inc:159
msgid "Package repository"
msgstr "Paketrepositorium"

#: admin/systems/services/repository/class_serviceRepository.inc:160
msgid "Package repository information"
msgstr "Paketrepositoriumsinformation"

#: admin/systems/services/repository/class_serviceRepository.inc:173
msgid "Repositories"
msgstr "Repositories"

#: admin/systems/services/repository/class_serviceRepository.inc:178
msgid "Repositories this server hosts"
msgstr "Repositories, die dieser Server bereitstellt"

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:26
msgid "Argonaut FAI monitor"
msgstr ""

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:27
#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:39
msgid "Argonaut FAI monitor settings"
msgstr ""

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:42
msgid "Port"
msgstr "Anschluss"

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:42
msgid "The port argonaut-fai-monitor should listen on. Default is 4711."
msgstr ""

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:47
msgid "Timeout"
msgstr "Wartezeit"

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:47
msgid "The timeout for bad clients. 0 to disable."
msgstr ""

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:52
msgid "CA certificate"
msgstr "CA-Zertifikat"

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:52
msgid "Path to the CA certificate file on Argonaut FAI monitor server"
msgstr ""

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:57
msgid "Log directory"
msgstr "Log Verzeichnis"

#: admin/systems/services/monitor/class_argonautFAIMonitor.inc:57
msgid "Directory in which argonaut-fai-monitor will store its log"
msgstr ""

#: config/fai/class_faiConfig.inc:28
msgid "FAI configuration"
msgstr "FAI Konfiguration"

#: config/fai/class_faiConfig.inc:29
msgid "FusionDirectory fai plugin configuration"
msgstr "FusionDirectory fai Plugin-Konfiguration"

#: config/fai/class_faiConfig.inc:45
msgid "Fai base RDN"
msgstr "Fai Basis RDN"

#: config/fai/class_faiConfig.inc:45
msgid "Branch in which fai branches will be stored"
msgstr "Zweig in welchem FAI-Zweige gespeichert werden"

#: config/fai/class_faiConfig.inc:60
#, php-format
msgid "Fai %s RDN"
msgstr "Fai %s RDN"

#: config/fai/class_faiConfig.inc:61
#, php-format
msgid "Relative branch in which fai %s will be stored"
msgstr "Relativer Zweig in welchem fai %s gespeichert werden"

#: config/fai/class_faiConfig.inc:61
msgid "s"
msgstr "s"

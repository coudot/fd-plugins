# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# Paola Penati <paola.penati@opensides.be>, 2019
# Paola <paola.penati@fusiondirectory.org>, 2019
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 20:05+0000\n"
"Last-Translator: Paola <paola.penati@fusiondirectory.org>, 2019\n"
"Language-Team: Italian (Italy) (https://www.transifex.com/fusiondirectory/teams/12202/it_IT/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it_IT\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/user-reminder/class_userReminderConfig.inc:27
msgid "Reminder"
msgstr "Promemoria"

#: config/user-reminder/class_userReminderConfig.inc:28
msgid "User reminder"
msgstr "Promemoria per l'utente"

#: config/user-reminder/class_userReminderConfig.inc:29
msgid "Configuration for the reminder of accounts expiration"
msgstr "Configurazione del promemoria della scadenza degli accounts"

#: config/user-reminder/class_userReminderConfig.inc:42
msgid "User reminder settings"
msgstr "Impostazioni promemoria per l'utente"

#: config/user-reminder/class_userReminderConfig.inc:45
msgid "Delay before expiration"
msgstr "Ritardo prima della scadenza"

#: config/user-reminder/class_userReminderConfig.inc:45
msgid "Days before expiration to send the first mail"
msgstr "Giorni prima della scadenza per inviare la prima e-mail"

#: config/user-reminder/class_userReminderConfig.inc:50
msgid "Delay before sending again"
msgstr "Richiamo prima di inviare di nuovo "

#: config/user-reminder/class_userReminderConfig.inc:50
msgid "Days before sending a second mail (0 to disable)"
msgstr "Giorni prima di inviare una seconda e-mail (0 per disabilitare)"

#: config/user-reminder/class_userReminderConfig.inc:55
msgid "Extension of the validity"
msgstr "Estensione della validità"

#: config/user-reminder/class_userReminderConfig.inc:55
msgid "Extension of the duration of the account in days"
msgstr "Estensione della durata dell'account in giorni"

#: config/user-reminder/class_userReminderConfig.inc:60
msgid "Sender email address"
msgstr "Indirizzo email del mittente"

#: config/user-reminder/class_userReminderConfig.inc:60
msgid "Email address from which mails will be sent"
msgstr "Indirizzo e-mail da cui la mail sarà inviata"

#: config/user-reminder/class_userReminderConfig.inc:65
msgid "Allow use of alternate addresses"
msgstr "Consenti l'uso di indirizzi alternativi"

#: config/user-reminder/class_userReminderConfig.inc:65
msgid ""
"Whether the field gosaMailAlternateAddress should be used as well to send "
"reminders"
msgstr ""
"Se il campo gosaMailAlternateAddress deve essere utilizzato anche per "
"inviare promemoria"

#: config/user-reminder/class_userReminderConfig.inc:72
msgid "Ppolicy email settings"
msgstr "Impostazioni email Ppolicy"

#: config/user-reminder/class_userReminderConfig.inc:75
#: config/user-reminder/class_userReminderConfig.inc:101
msgid "Forward alerts to the manager"
msgstr "Trasmettere le allerte al manager"

#: config/user-reminder/class_userReminderConfig.inc:75
msgid "Forward ppolicy alerts to the manager"
msgstr "Inoltrare avvisi al manager"

#: config/user-reminder/class_userReminderConfig.inc:80
#: config/user-reminder/class_userReminderConfig.inc:106
#: config/user-reminder/class_userReminderConfig.inc:132
#: config/user-reminder/class_userReminderConfig.inc:163
msgid "Subject"
msgstr "Oggetto"

#: config/user-reminder/class_userReminderConfig.inc:80
msgid "Subject of the ppolicy alert email"
msgstr "Oggetto della e-mail di avviso ppolicy"

#: config/user-reminder/class_userReminderConfig.inc:82
msgid "[FusionDirectory] Your password is about to expire"
msgstr "[FusionDirectory] La tua password sta per scadere"

#: config/user-reminder/class_userReminderConfig.inc:85
#: config/user-reminder/class_userReminderConfig.inc:137
#: config/user-reminder/class_userReminderConfig.inc:168
#, php-format
msgid "Body (%s are cn and login)"
msgstr "Corpo del messaggio (%s sono cn e login)"

#: config/user-reminder/class_userReminderConfig.inc:86
#, php-format
msgid ""
"Body of the ppolicy alert email, sent when the user password is about to "
"expire. Use %s for the cn and uid."
msgstr ""
"Corpo dell'email di avviso ppolicy, inviata quando la password dell'utente "
"sta per scadere. Usa %s per cn e uid."

#: config/user-reminder/class_userReminderConfig.inc:88
#, php-format
msgid ""
"Dear %1$s,\n"
"your password for account %2$s is about to expire, please change your password: \n"
"https://"
msgstr ""
"Gentile %1$s,\n"
" la tua password per l'account %2$s sta per scadere, ti preghiamo di cambiare la password: \n"
"https: //"

#: config/user-reminder/class_userReminderConfig.inc:98
msgid "Alert email settings"
msgstr "Impostazioni notifica e-mail"

#: config/user-reminder/class_userReminderConfig.inc:106
msgid "Subject of the alert email"
msgstr "Oggetto della notifica e-mail"

#: config/user-reminder/class_userReminderConfig.inc:108
msgid "[FusionDirectory] Your account is about to expire"
msgstr "[FusionDirectory] Il tuo account sta per scadere"

#: config/user-reminder/class_userReminderConfig.inc:111
#, php-format
msgid "Body (%s are cn, login, and link token)"
msgstr "Corpo del messaggio (%s sono cn, login e link token)"

#: config/user-reminder/class_userReminderConfig.inc:112
#, php-format
msgid ""
"Body of the alert email, sent when the user is about to expire. Use %s for "
"the cn, uid and token."
msgstr ""
"Corpo del messaggio dell'e-mail di notifica inviato quando l'utente sta per "
"scadere. Usare %s per il cn, uid e token."

#: config/user-reminder/class_userReminderConfig.inc:114
#, php-format
msgid ""
"Dear %1$s,\n"
"your account %2$s is about to expire, please use this link to avoid this: \n"
"https://"
msgstr ""
"Caro %1$s,\n"
"il tuo account %2$s sta per scadere, si prega di utilizzare questo link per evitare questo: \n"
"https://"

#: config/user-reminder/class_userReminderConfig.inc:124
msgid "Confirmation email settings"
msgstr "Conferma delle impostazioni e-mail"

#: config/user-reminder/class_userReminderConfig.inc:127
msgid "Forward confirmation to the manager"
msgstr "Trasmettere conferma al manager"

#: config/user-reminder/class_userReminderConfig.inc:127
msgid "Forward account extension confirmation to the manager"
msgstr "Trasmettere conferma dell'estensione account al manager"

#: config/user-reminder/class_userReminderConfig.inc:132
msgid "Subject of the confirmation email"
msgstr "Oggetto dell'e-mail di conferma"

#: config/user-reminder/class_userReminderConfig.inc:134
msgid "[FusionDirectory] Your account expiration has been postponed"
msgstr ""
"[FusionDirectory] La data di scadenza del tuo account é stata rinviata"

#: config/user-reminder/class_userReminderConfig.inc:138
#, php-format
msgid ""
"Body of the confirmation email, sent when the user has postponed expiration."
" Use %s for the cn and uid."
msgstr ""
"Corpo del messaggio di conferma, inviato quando l'utente ha rinviato la "
"scadenza. Usare %s per il CN e UID."

#: config/user-reminder/class_userReminderConfig.inc:140
#, php-format
msgid ""
"Dear %1$s,\n"
" your account %2$s expiration has been successfully postponed.\n"
msgstr ""
"Caro %1$s,\n"
"la data di scadenza del tuo acccount %2$s é stata rinviata con successo.\n"

#: config/user-reminder/class_userReminderConfig.inc:150
msgid "Expiration email settings"
msgstr "Impostazioni e-mail di scadenza"

#: config/user-reminder/class_userReminderConfig.inc:153
msgid "Send an email"
msgstr "Invia una mail"

#: config/user-reminder/class_userReminderConfig.inc:153
msgid "Whether to send an email after expiration to inform the user"
msgstr "Se inviare un'e-mail dopo la scadenza per informare l'utente"

#: config/user-reminder/class_userReminderConfig.inc:158
msgid "Forward to the manager"
msgstr "Inoltra al manager"

#: config/user-reminder/class_userReminderConfig.inc:158
msgid "Forward account expiration to the manager"
msgstr "Inoltra la scadenza dell'account al manager"

#: config/user-reminder/class_userReminderConfig.inc:163
msgid "Subject of the expiration email"
msgstr "Oggetto dell'email di scadenza"

#: config/user-reminder/class_userReminderConfig.inc:165
msgid "[FusionDirectory] Your account has expired"
msgstr "[FusionDirectory] Il tuo account è scaduto"

#: config/user-reminder/class_userReminderConfig.inc:169
#, php-format
msgid ""
"Body of the expiration email, sent when the user has expired. Use %s for the"
" cn and uid."
msgstr ""
"Corpo dell'e-mail di scadenza, inviato quando l'utente è scaduto. Usa %s per"
" cn e uid."

#: config/user-reminder/class_userReminderConfig.inc:171
#, php-format
msgid ""
"Dear %1$s,\n"
" your account %2$s has expired.\n"
msgstr ""
"Caro %1$s,\n"
"il tuo acount %2$sé scaduto.\n"

#: html/class_expiredUserPostpone.inc:59
msgid "This token is invalid"
msgstr "Questo token non è valido"

#: html/class_expiredUserPostpone.inc:62
msgid "Token is missing from URL"
msgstr "Il token è mancante dall'URL"

#: html/class_expiredUserPostpone.inc:230
msgid "Contact your administrator, there was a problem with mail server"
msgstr ""
"Contattare l'amministratore, c'è stato un problema con il server di posta "
"elettronica"

#: html/class_expiredUserPostpone.inc:239
#, php-format
msgid "Did not find an account with login \"%s\""
msgstr "Alcun account trovato con login \"%s\" "

#: html/class_expiredUserPostpone.inc:242
#, php-format
msgid "Found multiple accounts with login \"%s\""
msgstr "Trovati più accounts con login \"%s\""

#: ihtml/themes/breezy/user-reminder.tpl.c:2
#: ihtml/themes/breezy/user-reminder.tpl.c:5
msgid "User"
msgstr "Utente"

#: ihtml/themes/breezy/user-reminder.tpl.c:8
msgid "Expiration postpone"
msgstr "Rinvio scadenza"

#: ihtml/themes/breezy/user-reminder.tpl.c:11
#: ihtml/themes/breezy/user-reminder.tpl.c:14
msgid "Success"
msgstr "Completato"

#: ihtml/themes/breezy/user-reminder.tpl.c:17
msgid "Your expiration has been postponed successfully."
msgstr "La vostra scadenza é stata rinviata con successo."

#: ihtml/themes/breezy/user-reminder.tpl.c:20
#: ihtml/themes/breezy/user-reminder.tpl.c:23
msgid "Error"
msgstr "Errore"

#: ihtml/themes/breezy/user-reminder.tpl.c:26
msgid "There was a problem"
msgstr "C'é stato un problema"

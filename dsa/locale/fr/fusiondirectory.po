# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# Benoit Mortier <benoit.mortier@fusiondirectory.org>, 2019
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:53+0000\n"
"Last-Translator: Benoit Mortier <benoit.mortier@fusiondirectory.org>, 2019\n"
"Language-Team: French (https://www.transifex.com/fusiondirectory/teams/12202/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: admin/dsa/class_dsaManagement.inc:26 admin/dsa/class_dsaManagement.inc:32
#: config/dsa/class_dsaConfig.inc:40
msgid "DSA"
msgstr "DSA"

#: admin/dsa/class_dsaManagement.inc:27
msgid "DSA management"
msgstr "Gestion DSA"

#: admin/dsa/class_dsaManagement.inc:28
msgid "Manage simple security objects"
msgstr "Gérer des objets de sécurité simples"

#: admin/dsa/class_simpleSecurityObject.inc:26
#: admin/dsa/class_simpleSecurityObject.inc:27
#: admin/dsa/class_simpleSecurityObject.inc:31
#: admin/dsa/class_simpleSecurityObject.inc:47
msgid "Simple security object"
msgstr "Objet de sécurité simple"

#: admin/dsa/class_simpleSecurityObject.inc:51
msgid "Entry name"
msgstr "Nom"

#: admin/dsa/class_simpleSecurityObject.inc:51
msgid "Account name"
msgstr "Nom du compte"

#: admin/dsa/class_simpleSecurityObject.inc:55
msgid "Description"
msgstr "Description"

#: admin/dsa/class_simpleSecurityObject.inc:55
msgid "Description of this simple security object"
msgstr "Description de ce compte DSA"

#: admin/dsa/class_simpleSecurityObject.inc:61
msgid "Change password"
msgstr "Modifier le mot de passe"

#: admin/dsa/class_simpleSecurityObject.inc:64
msgid "Password"
msgstr "Mot de passe"

#: config/dsa/class_dsaConfig.inc:26
msgid "DSA configuration"
msgstr "Configuration de DSA"

#: config/dsa/class_dsaConfig.inc:27
msgid "FusionDirectory dsa plugin configuration"
msgstr "Configuration du plugin dsa"

#: config/dsa/class_dsaConfig.inc:43
msgid "DSA RDN"
msgstr "Branche DSA"

#: config/dsa/class_dsaConfig.inc:43
msgid "Branch in which Directory Service Account (dsa) will be stored"
msgstr ""
"Branche dans laquelle les comptes DSA (Directory Service Account) seront "
"stockés"

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:59+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: German (https://www.transifex.com/fusiondirectory/teams/12202/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/systems/services/postfix/class_servicePostfix.inc:55
#: admin/systems/services/postfix/class_servicePostfix.inc:56
msgid "Postfix (SMTP)"
msgstr "Postfix (SMTP)"

#: admin/systems/services/postfix/class_servicePostfix.inc:56
msgid "Services"
msgstr "Dienste"

#: admin/systems/services/postfix/class_servicePostfix.inc:71
msgid "Information"
msgstr "Information"

#: admin/systems/services/postfix/class_servicePostfix.inc:74
msgid "Hostname"
msgstr "Hostname"

#: admin/systems/services/postfix/class_servicePostfix.inc:74
msgid "The host name."
msgstr "Der Hostname."

#: admin/systems/services/postfix/class_servicePostfix.inc:79
#: admin/systems/services/postfix/class_servicePostfix.inc:131
msgid "Domain"
msgstr "Domäne"

#: admin/systems/services/postfix/class_servicePostfix.inc:79
msgid "The domain."
msgstr "Die Domain."

#: admin/systems/services/postfix/class_servicePostfix.inc:84
msgid "Max mail header size (KB)"
msgstr "Max. Mailheadergröße (KB)"

#: admin/systems/services/postfix/class_servicePostfix.inc:84
msgid "This value specifies the maximal header size."
msgstr "Dieser Wert legt die maximale Größe der Kopfzeilen fest."

#: admin/systems/services/postfix/class_servicePostfix.inc:89
msgid "Max mailbox size (KB)"
msgstr "Max. Mailboxgröße (KB)"

#: admin/systems/services/postfix/class_servicePostfix.inc:89
msgid "Defines the maximal size of mail box."
msgstr "Setzt die maximale Größe der Mailbox."

#: admin/systems/services/postfix/class_servicePostfix.inc:94
msgid "Max message size (KB)"
msgstr "Max. Nachrichtengröße (KB)"

#: admin/systems/services/postfix/class_servicePostfix.inc:94
msgid "Specify the maximal size of a message."
msgstr "Setzt die maximale Größe einer Nachricht."

#: admin/systems/services/postfix/class_servicePostfix.inc:101
msgid "Domains and routing"
msgstr "Domänen und Weiterleitung"

#: admin/systems/services/postfix/class_servicePostfix.inc:105
msgid "Domains to accept mail for"
msgstr "Domänen, für die Mail angenommen wird"

#: admin/systems/services/postfix/class_servicePostfix.inc:105
msgid "Postfix networks"
msgstr "Postfix-Netzwerke"

#: admin/systems/services/postfix/class_servicePostfix.inc:110
msgid "Transport table"
msgstr "Transport-Tabelle"

#: admin/systems/services/postfix/class_servicePostfix.inc:115
msgid "Transport rule"
msgstr "Transportregel"

#: admin/systems/services/postfix/class_servicePostfix.inc:127
msgid "Catchall table"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:131
msgid "Domain concerned by this catchall rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:135
msgid "Recipient"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:135
msgid "Recipient mail address for this catchall rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:141
msgid "Domain alias table"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:145
msgid "From"
msgstr "Von"

#: admin/systems/services/postfix/class_servicePostfix.inc:145
msgid "Domain concerned by this alias rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:149
msgid "To"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:149
msgid "Recipient domain for this alias rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:157
msgid "Restrictions"
msgstr "Einschränkungen"

#: admin/systems/services/postfix/class_servicePostfix.inc:161
msgid "Restrictions for sender"
msgstr "Einschränkungen für den Sender"

#: admin/systems/services/postfix/class_servicePostfix.inc:170
msgid "For sender"
msgstr "Für Absender"

#: admin/systems/services/postfix/class_servicePostfix.inc:175
msgid "Restrictions for recipient"
msgstr "Einschränkungen für Empfänger"

#: admin/systems/services/postfix/class_servicePostfix.inc:184
msgid "For recipient"
msgstr "Für Empfänger"

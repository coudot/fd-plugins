# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:57+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2020\n"
"Language-Team: Czech (Czech Republic) (https://www.transifex.com/fusiondirectory/teams/12202/cs_CZ/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs_CZ\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: admin/groups/mail/class_mailGroup.inc:46
#: config/mail/class_mailPluginConfig.inc:26
#: personal/mail/class_mailAccount.inc:73
msgid "Mail"
msgstr "E-mail"

#: admin/groups/mail/class_mailGroup.inc:47
msgid "Group mail options"
msgstr "Předvolby skupinového e-mailu"

#: admin/groups/mail/class_mailGroup.inc:62
msgid "Information"
msgstr "Informace"

#: admin/groups/mail/class_mailGroup.inc:65
#: personal/mail/class_mailAccount.inc:101
msgid "Primary address"
msgstr "Hlavní adresa"

#: admin/groups/mail/class_mailGroup.inc:65
msgid "The primary mail address"
msgstr "Hlavní e-mailová adresa"

#: admin/groups/mail/class_mailGroup.inc:69
#: personal/mail/class_mailAccount.inc:105
msgid "Server"
msgstr "Server"

#: admin/groups/mail/class_mailGroup.inc:69
msgid "Email server"
msgstr "E-mailový server"

#: admin/groups/mail/class_mailGroup.inc:75
#: personal/mail/class_mailAccount.inc:121
msgid "Alternative addresses"
msgstr "Alternativní adresy"

#: admin/groups/mail/class_mailGroup.inc:75
msgid "Alternative mail addresses for the group"
msgstr "Alternativní e-mailové adresy této skupiny"

#: admin/groups/mail/class_mailGroup.inc:82
msgid "Options"
msgstr "volby"

#: admin/groups/mail/class_mailGroup.inc:85
msgid "Forward messages to non group members"
msgstr "přeposlat zprávy těm, kdo nejsou členy skupiny"

#: admin/groups/mail/class_mailGroup.inc:89
msgid "Only allowed to receive local mail"
msgstr "Může přijímat pouze místní e-maily"

#: admin/groups/mail/class_mailGroup.inc:89
msgid ""
"Whether this group mail is only allowed to receive messages from local "
"senders"
msgstr ""
"Zda je této e-mailové skupině omezeno přijímání zpráv pouze na ty od "
"místních odesilatelů"

#: admin/groups/mail/class_mailGroup.inc:94
msgid "Default ACL"
msgstr ""

#: admin/groups/mail/class_mailGroup.inc:94
msgid "ACL for people not in the group"
msgstr ""

#: admin/groups/mail/class_mailGroup.inc:98
msgid "Members ACL"
msgstr ""

#: admin/groups/mail/class_mailGroup.inc:98
msgid "ACL for members of the group"
msgstr ""

#: admin/groups/mail/class_mailGroup.inc:103
msgid "Special ACLs for specific members"
msgstr ""

#: admin/groups/mail/class_mailGroup.inc:107
msgid "Email"
msgstr "E-mail"

#: admin/groups/mail/class_mailGroup.inc:112
msgid "ACL"
msgstr "přístupová práva (ACL)"

#: admin/groups/mail/class_mailGroup.inc:118
msgid "Special ACLs"
msgstr ""

#: admin/groups/mail/class_mailGroup.inc:296
msgid "Error"
msgstr "Chyba"

#: admin/groups/mail/class_mailGroup.inc:297
#, php-format
msgid "Several users have uid \"%s\". Ignoring this member."
msgstr "Několik uživatelů má uid „%s“. Toto číslo bude ignorováno."

#: admin/groups/mail/class_mailGroup.inc:343
#: personal/mail/class_mailAccount.inc:269
#: personal/mail/class_mailAccount.inc:370
#, php-format
msgid "Mail method cannot connect: %s"
msgstr "E-mailová metodou se nelze připojit: %s"

#: admin/groups/mail/class_mailGroup.inc:346
#: personal/mail/class_mailAccount.inc:373
#, php-format
msgid "Cannot update mailbox: %s"
msgstr "Nezdařilo se aktualizovat schránku: %s"

#: admin/groups/mail/class_mailGroup.inc:383
#: admin/groups/mail/class_mailGroup.inc:387
#: personal/mail/class_mailAccount.inc:32
#: personal/mail/class_mailAccount.inc:269
#: personal/mail/class_mailAccount.inc:272
#: personal/mail/class_mailAccount.inc:376
#: personal/mail/class_mailAccount.inc:387
#: personal/mail/class_mailAccount.inc:416
#: personal/mail/class_mailAccount.inc:420
msgid "Mail error"
msgstr "chyba e-mailu"

#: admin/groups/mail/class_mailGroup.inc:383
#: personal/mail/class_mailAccount.inc:416
#, php-format
msgid "Cannot remove mailbox, mail method cannot connect: %s"
msgstr "E-mailovou schránku se nedaří odebrat, nedaří se připojit metodou: %s"

#: admin/groups/mail/class_mailGroup.inc:387
#: personal/mail/class_mailAccount.inc:420
#, php-format
msgid "Cannot remove mailbox: %s"
msgstr "Nelze odebrat schárnku: %s"

#: admin/systems/services/imap/class_serviceIMAP.inc:52
msgid "IMAP generic service"
msgstr ""

#: admin/systems/services/imap/class_serviceIMAP.inc:53
msgid "IMAP"
msgstr ""

#: admin/systems/services/imap/class_serviceIMAP.inc:53
msgid "Services"
msgstr "Služby"

#: config/mail/class_mailPluginConfig.inc:27
msgid "Mail plugin configuration"
msgstr "Nastavení zásuvného modulu E-mail"

#: config/mail/class_mailPluginConfig.inc:40
#: personal/mail/class_mailAccount.inc:74
msgid "Mail settings"
msgstr "nastavení e-mailu"

#: config/mail/class_mailPluginConfig.inc:43
msgid "Account identification attribute"
msgstr "atribut, dle kterého budou rozlišovány účty"

#: config/mail/class_mailPluginConfig.inc:44
msgid "Which attribute will be used to create accounts."
msgstr "Který atribut má být použit pro vytváření účtů."

#: config/mail/class_mailPluginConfig.inc:49
msgid "User account template"
msgstr ""

#: config/mail/class_mailPluginConfig.inc:50
msgid ""
"Override the user account creation syntax. Default is %PREFIX%%UATTRIB%."
msgstr ""

#: config/mail/class_mailPluginConfig.inc:54
msgid "Group account template"
msgstr ""

#: config/mail/class_mailPluginConfig.inc:55
msgid ""
"Override the group account creation syntax. Default is %PREFIX%%UATTRIB%."
msgstr ""

#: config/mail/class_mailPluginConfig.inc:59
msgid "Delete mailbox on account deletion"
msgstr "Při smazání účtu smazat i e-mailovou schránku"

#: config/mail/class_mailPluginConfig.inc:60
msgid ""
"Determines if the mailbox should be removed from your IMAP server after the "
"account is deleted in LDAP."
msgstr ""
"Určuje, zda má být po smazání účtu v LDAP též odebrána e-mailová schránka na"
" IMAP serveru."

#: config/mail/class_mailPluginConfig.inc:64
msgid "IMAP timeout"
msgstr "Časový limit pro IMAP"

#: config/mail/class_mailPluginConfig.inc:65
msgid "Sets the connection timeout for imap actions."
msgstr "Nastavuje časové limity prodlevy připojení pro akce imap."

#: config/mail/class_mailPluginConfig.inc:70
msgid "Shared prefix"
msgstr "Sdílená předpona"

#: config/mail/class_mailPluginConfig.inc:71
msgid "Prefix to add for mail shared folders."
msgstr "Předpona, kterou přidat pro sdílené poštovní složky."

#: personal/mail/class_mailAccount.inc:32
#, php-format
msgid "Cannot read quota settings: %s"
msgstr "nelze načíst nastavení kvóty: %s"

#: personal/mail/class_mailAccount.inc:42
msgid "Quota usage"
msgstr "Využití kvóty"

#: personal/mail/class_mailAccount.inc:43
msgid "Part of the quota which is used"
msgstr "Část kvóty, která je využita"

#: personal/mail/class_mailAccount.inc:98
msgid "Mail account"
msgstr "E-mailový účet"

#: personal/mail/class_mailAccount.inc:101
msgid "Primary mail address"
msgstr "Hlavní e-mailová adresa"

#: personal/mail/class_mailAccount.inc:105
msgid "Specify the mail server where the user will be hosted on"
msgstr "Zvolte poštovní server, na kterém bude uživatel mít svůj účet."

#: personal/mail/class_mailAccount.inc:110
msgid "Quota size"
msgstr "kvóta objemu dat"

#: personal/mail/class_mailAccount.inc:110
msgid "Define quota size in MiB"
msgstr ""

#: personal/mail/class_mailAccount.inc:117
msgid "Other addresses and redirections"
msgstr "Ostatní adresy a přesměrování"

#: personal/mail/class_mailAccount.inc:121
msgid "List of alternative mail addresses"
msgstr "seznam alternativních e-mailových adres"

#: personal/mail/class_mailAccount.inc:126
msgid "Forward messages to"
msgstr "přesměrovat zprávy na"

#: personal/mail/class_mailAccount.inc:126
msgid "Addresses to which messages should be forwarded"
msgstr "Adresy, na které by měly být přeposílány zprávy"

#: personal/mail/class_mailAccount.inc:132
#: personal/mail/class_mailAccount.inc:151
msgid "Vacation message"
msgstr "zpráva „jsem na dovolené“"

#: personal/mail/class_mailAccount.inc:135
msgid "Activate vacation message"
msgstr "aktivovat zprávu „jsem na dovolené“"

#: personal/mail/class_mailAccount.inc:136
msgid ""
"Select to automatically response with the vacation message defined below"
msgstr ""
"zaškrtněte pro automatické odpovídání níže uvedenou zprávou „jsem na "
"dovolené“"

#: personal/mail/class_mailAccount.inc:141
msgid "from"
msgstr "od"

#: personal/mail/class_mailAccount.inc:146
msgid "till"
msgstr "dokud"

#: personal/mail/class_mailAccount.inc:157
msgid "Advanced mail options"
msgstr "pokročilé volby e-mailu"

#: personal/mail/class_mailAccount.inc:165
msgid "User is only allowed to send and receive local mails"
msgstr "uživateli je dovoleno přijímat a odesílat pouze místní poštu"

#: personal/mail/class_mailAccount.inc:166
msgid "Select if user can only send and receive inside his own domain"
msgstr ""
"zvolte, zda uživatel bude moci přijímat a odesílat pouze v rámci vlastní "
"domény"

#: personal/mail/class_mailAccount.inc:171
msgid "No delivery to own mailbox"
msgstr "nedoručovat do vlastní schránky"

#: personal/mail/class_mailAccount.inc:172
msgid "Select if you want to forward mails without getting own copies of them"
msgstr ""
"Zvolte, zda chcete přeposílat zprávy bez toho, abyste si ponechávali jejich "
"kopie."

#: personal/mail/class_mailAccount.inc:272
#, php-format
msgid "Mailbox \"%s\" doesn't exists on mail server: %s"
msgstr "Schránka „%s“ neexistuje na e-mailovém serveru: %s"

#: personal/mail/class_mailAccount.inc:376
#, php-format
msgid "Cannot write quota settings: %s"
msgstr "Nezdařilo se zapsat nastavení kvóty: %s"

#: personal/mail/class_mailAccount.inc:387
#, php-format
msgid "Mail error saving sieve settings: %s"
msgstr "Chyba e-mailu při ukládání sieve nastavení: %s"

#: personal/mail/class_mail-methods.inc:138
msgid "Configuration error"
msgstr "Chyba v nastavení"

#: personal/mail/class_mail-methods.inc:139
#, php-format
msgid "The configured mail attribute '%s' is unsupported!"
msgstr "Nastavený atribut zprávy '%s' není podporován!"

#: personal/mail/class_mail-methods.inc:145
#, php-format
msgid "%s (%s)"
msgstr "%s (%s)"

#: personal/mail/class_mail-methods.inc:585
msgid "There are no IMAP compatible mail servers defined!"
msgstr "Nejsou určeny žádné poštovní servery slučitelné s protokolem IMAP! "

#: personal/mail/class_mail-methods.inc:590
msgid "Mail server for this account is invalid!"
msgstr "Poštovní server pro tento účet je neplatný!"

#: personal/mail/class_mail-methods.inc:707
msgid "Unknown"
msgstr "Neznámé"

#: personal/mail/class_mail-methods.inc:709
msgid "Unlimited"
msgstr "Neomezené"
